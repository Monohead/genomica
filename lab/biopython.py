from Bio import SeqIO
import numpy
# Abrimos el archivo
f = open("lacto-HM.gb")
# obtenemos el genoma 
genome = SeqIO.read(f, "genbank")

# Obtener el id
print genome.id
# Obtener la descripcion
print genome.description
# Obtener el nombre
print genome.name
# Obtener la secuencias
genome.seq
# Obtener la secuencia del caracter 0 hasta el 30
genome.seq[1:30]
# Obtener el complemento del indicce 500 al 530
print genome.seq[500:530].reverse_complement()

# Esctructura del genoma
# print dir(genome)


# Obtener la informacion para cada uno de los genes.
# print genome.features

# Obtener la informacion de los genes de forma ordenada

prom = 0
ncds = 0
pgcg = 0.0

for i in genome.features:
    if(i.type == 'CDS'):
        ncds += 1
        x = i.location.start
        y = i.location.end
        prom += y-x
        pseq = genome.seq[x:y]
        nc = pseq.count('C')
        ng = pseq.count('G')
        pcg = float((nc+ng))/len(pseq)
        pgcg += pcg
        # Obtiene la cadena en la que se encuentra.
        print i.strand
        print x,y
        print y-x
        #print dir(i)
        
print 'Promedio de longitud', prom/ncds
print 'Promedio %GC de los CDS', pgcg/ncds

gpcg = float(genome.seq.count('C') + genome.seq.count('G')) / len(genome.seq)

print 'Promedio GC de la secuencia', gpcg

# print len(genome.features)


####################################################################

# El gen hace referencia a las regiones que puede ser transcribe

# CDS - Coding Sequence, este hace referencia a la region que se traduce

####################################################################

# Obtenemos el numero de genes sobre las dos secuencias
ngs = 0 # Sobre la secuencia
ngc = 0 # Sobre el complemento

for j in genome.features:
    if(j.strand == 1):
        ngs += 1
    if(j.strand == -1):
        ngc += 1

