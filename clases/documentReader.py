def __main__():
    f = open("sequence.fasta")

    secuencia = ''

    for line in f.readlines():
        if line.count('>') == 0:
            secuencia += line.strip()

    lon = len(secuencia) * 1.0
    na = count(secuencia,'A')
    ng = count(secuencia,'G')
    nc = count(secuencia,'C')
    nt = count(secuencia,'T')
    pcg = ((ng+nc)/lon)
    print ("-------Datos generales-------")
    print ("Numero de A's: " + str(na))
    print ("Numero de G's: " + str(ng))
    print ("Numero de C's: " + str(nc))
    print ("Numero de T's: " + str(nt))
    print ("%GC: " + str(pcg)) 
    print ("Tamano de la cadena: " + str(lon))

    print ("-----------1er Gen-----------")
    gpgc = analizar(secuencia,412,736)
    print ("%GC Gen : ") + str(gpgc)



def analizar(secuencia,posi,posf):
    ca = secuencia[posi:posf]
    alon = len(ca) * 1.0
    anc = count(ca,'C')
    ang = count(ca,'G')
    pgc = (anc+ang) / alon
    return pgc

def count(secuencia, caracter):
    na = 0
    for char in range(0, len(secuencia)):
        if secuencia[char] == caracter:
            na += 1
    return na

if __name__ == '__main__':
	__main__()
