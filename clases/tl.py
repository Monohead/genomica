from numpy import array, dot, radians
from numpy.linalg import eigh
from matplotlib import pyplot as pit
#matplotlib inline

A = array([[2,0],[0,1]])
print A
w = array([0.5,0.5])

v = A.dot(w)
print v

L, V = eigh(A)

print(L)
print(V)
