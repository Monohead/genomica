def __main__():
    
    ecoli = recupera_adn('phagelambda.fasta')
    comp = complemento(ecoli)
    print "Tamano en pares de bases:", len(ecoli)
    pcg = porcentaje_gc(ecoli)
    print "El porcentaje GC es:", pcg
    pnt = proporcion_nt(ecoli)
    pntc = proporcion_nt(comp)
    pa = pnt['A']
    pt = pnt['T']
    pc = pnt['C']
    pg = pnt['G']
    pca = pntc['A']
    pct = pntc['T']
    pcc = pntc['C']
    pcg = pntc['G']
    cu = "AGATCTCGAT"
    cd = "GGGCCC"
    print 'La proporcion de los nucleotidos es:'
    for i in pnt:
        print i,pnt[i]

    su = (pa*pg*pa*pt*pc*pt*pc*pg*pa*pt)
    #su = (pa*pg*pa*pt*pc*pt*pc*pg*pa*pt) * (pca*pcg*pca*pct*pcc*pct*pcc*pcg*pca*pct)

    sd = pg*pg*pg*pc*pc*pc

    print "La probabilidad de la primera cadena es", su
    print "La probabilidad de la segunda cadena es", sd

    ncu = 0
    ncu = contar_codon(ecoli,cu)
    ncu += contar_codon(comp,cu)
    print "El numero de apariciones de", cu, "es", ncu

    ncd = 0
    ncd = contar_codon(ecoli,cd)
    #ncd = contar_codon(ecoli,cd) tambien da uno

    print "El numero de apariciones de", cd, "es", ncd


# Funcion que regresa la secuencia de ADN del archivo en un String

def recupera_adn(archivo):

    f = open(archivo)
    secuencia = ''
    
    for line in f.readlines():
        if line.count('>') == 0:
            secuencia += line.strip()

    return secuencia

# Funcion que toma una secuencia y regresa una lista de subcadenas de tamano n

def agrupa_en_n(secuencia, n = 3):
    
    lista = [secuencia[i:i+n] for i in range(0, len(secuencia), n)]
    return lista

# Funcion que regresa la reversa complementaria de una secuencia

def complemento(secuencia):

    reversa = ''
    
    for i in range (0, len(secuencia)):
        if(secuencia[i] == 'A'):
            reversa += 'T'
        if(secuencia[i] == 'T'):
            reversa += 'A'
        if(secuencia[i] == 'G'):
            reversa += 'C'
        if(secuencia[i] == 'C'):
            reversa += 'G'

    reversa = reversa[::-1]
    
    return reversa

# Funcion que regresa un diccionario con la proporcion de cada base

def proporcion_nt(secuencia):

    tamano = len(secuencia) * 1.0
    pa = secuencia.count('A') / tamano
    pt = secuencia.count('T') / tamano
    pc = secuencia.count('C') / tamano
    pg = secuencia.count('G') / tamano

    dic = {}
    dic['A'] = pa
    dic['T'] = pt
    dic['C'] = pc
    dic['G'] = pg
    return dic

# Funcion que regresa el porcentage gc de la secuencia dada

def porcentaje_gc(secuencia):

    tamano = len(secuencia) * 1.0
    pgc = (secuencia.count('C') + secuencia.count('G')) / tamano
    return pgc

# Funcion que regresa el numero de veces que aparece el codon dado

def contar_codon(secuencia, codon):

    ncodon = secuencia.count(codon)
    return ncodon

# Funcion que regresa la cadena de ARN a partir  de la secuencia dada

def transcripcion(secuencia):

    arn = ''

    for i in range(0, len(secuencia)):
        if(secuencia[i] == 'T'):
            arn += 'U'
        else:
            arn += secuencia[i]

    return arn

#Funcion que busca la subsecuencia de ARN que corresponde al primer ORF

def encuentra_orf(secuencia_arn_completa):
    inicio = encuentrai(secuencia_arn_completa)
    o,s = encuentraf(inicio)
    return o,s

def encuentrai(secuencia):
    m = len(secuencia)
    j = secuencia.find('ATG')
    print j
    return secuencia[j:m]    

def encuentraf(secuencia): 
    codones = agrupa_en_n(secuencia)
    n = len(codones)
    t = 0
    for i in range(0,n):
        o = codones[i]
        t += 3
        if((o == 'TGA') | (o == 'TAG') | (o == 'TAA')) & (t >= 50) & (t <= 3500):
            return secuencia[0:t], secuencia[t:n]

# Funcion que busca todos los ORF en todo el genoma

def todo_orf(secuencia):
    comp = complemento(secuencia)
    orfs = [[]] * 100
    i = 0
    while(len(secuencia) > 50):
        orf, sub = encuentra_orf(secuencia)
        orfs[i] = orf
        i += 1
        secuencia = sub
    while(len(comp) > 50):
        orf, sub = encuentra_orf(comp)
        orfs[i] = orf
        i += 1
        comp = sub
    return orfs
    
# Funcion que regresa una cadena de aminoacidos correspondientes a la traduccion de la secuencia
# usando el codigo genetico

def traduccion(secuencia_arn_leida):

    aminoacidos = ''
    codones = agrupa_en_n(secuencia_arn_leida)

    alanina = ['GCU','GCC', 'GCA', 'GCG']
    arginina = ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG']
    asparagina = ['AAU', 'AAC']
    aspartato = ['GAU', 'GAC']
    cisteina = ['UGU', 'UGC']
    glutamina = ['CAA', 'CAG']
    glutamato = ['GAA', 'GAG']
    glicina = ['GGU', 'GGC', 'GGA', 'GGG']
    histidina = ['CAU', 'CAC']
    isoleucina = ['AUU', 'AUC', 'AUA']
    leucina = ['UUA', 'UUG', 'CUU', 'CUC', 'CUA', 'CUG']
    lisina = ['AAA', 'AAG']
    metionina = ['AUG']
    fenilalanina = ['UUU', 'UUC']
    prolina = ['CCU', 'CCC', 'CCA', 'CCG']
    #selenocisteina = ['UGA']
    serina = ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC']
    treonina = ['ACU', 'ACC', 'ACA', 'ACG']
    triptofano = ['UGG']
    tirosina = ['UAU', 'UAC']
    valina = ['GUU', 'GUC', 'GUA', 'GUG']

    for i in range(0, len(codones)):
        c = codones[i]
        if c in alanina:
            aminoacidos += 'A'
        if c in arginina:
            aminoacidos += 'R'
        if c in asparagina:
            aminoacidos += 'N'
        if c in aspartato:
            aminoacidos += 'D'
        if c in cisteina:
            aminoacidos += 'C'
        if c in glutamina:
            aminoacidos += 'Q'
        if c in glutamato:
            aminoacidos += 'E'
        if c in glicina:
            aminoacidos += 'G'
        if c in histidina:
            aminoacidos += 'H'
        if c in isoleucina:
            aminoacidos += 'I'
        if c in leucina:
            aminoacidos += 'L'
        if c in lisina:
            aminoacidos += 'K'
        if c in metionina:
            aminoacidos += 'M'
        if c in fenilalanina:
            aminoacidos += 'F'
        if c in prolina:
            aminoacidos += 'P'
        if c in serina:
            aminoacidos += 'S'
        if c in treonina:
            aminoacidos += 'T'
        if c in triptofano:
            aminoacidos += 'W'
        if c in tirosina:
            aminoacidos += 'Y'
        if c in valina:
            aminoacidos += 'V'

    return aminoacidos



if __name__ == '__main__':
    __main__()
