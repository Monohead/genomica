import numpy as np
#####################
# Funcion principal #
#####################

def __main__():

    score = []
    alignmentA = ''
    alignmentB = ''
    x = ''
    y = ''
    

    ## DESCOMENTAR PARA VER MAS RAPIDO Y MENOS CODIGO ##
    
    ## PRIMER EJEMPLO ##
    
    #x = 'TGTTACGG'
    #y = 'GGTTGACTA'
    #print 'Ejemplo 1:\n',x,'\n',y

    ## EJERCICIO 7 (i) ##
    #x = 'GTAA'
    #y = 'CTTAGA'

    ## EJERCICIO 7 (ii) ##
    #x = 'GATTACACACA'
    #y = 'GATTTATCACAA'

    ## EJERCICIO 7 (iii) ##
    #x = 'ATGCATGCATGC'
    #y = 'ATGCATTATTATGCATGC'

    ## Genes de Myxococcus xanthus ##
    # Primer gen de 6624027 a 6624102
    #x = 'GGGGGTGTAGCTCAGTTGGGAGAGCGTCGCGTTCGCAATGCGAAGGTCGTCGGTTCGATCCCGTCCACCTCCACCA'
    # Segundo gen 6257408 a 6257494
    #y = 'GCCCAGGTGGTGGAATTGGTAGACACACTAGATTCAGGGTCTAGCGCCTGCAAGGGCATGGAGGTTCGAGTCCTCTCCTGGGCACAA'

    score,alignmentA,alignmentB = nw(x,y)
    print 'Needleman-Wunsch'
    print 'Score:',score[len(x),len(y)]
    print alignmentA
    print alignmentB
    score,alignmentA,alignmentB = sw(x,y)
    print 'Smith-Waterman'
    print 'Score',np.amax(score)
    print alignmentA
    print alignmentB

############################
# Funcion Needleman-Wunsch #
############################

def nw(secx, secy):
    n = len(secx)
    m = len(secy)
    d = -2
    
    score = np.zeros((n+1,m+1))

    for i in range(1,n+1):
        score[i,0] = score[i-1,0]-1
    for j in range(1,m+1):
        score[0,j] = score[0,j-1]-1


    for k in range(1,n+1):
        for l in range(1,m+1):
            
            match = score[k-1,l-1] + delta(secx[k-1],secy[l-1])
            delete = score[k-1,l] + d
            insert = score[k,l-1] + d
            score[k,l] = max(match,delete,insert)


    alignmentA = ''
    alignmentB = ''


    while(n > 0 and m > 0):
        if(n > 0 and m > 0 and score[n,m] == score[n-1,m-1] + delta(secx[n-1],secy[m-1])):
            alignmentA = secx[n-1] + alignmentA
            alignmentB = secy[m-1] + alignmentB
            n -= 1
            m -= 1
        elif(n > 0 and score[n,m] == score[n-1,m] + d):
            alignmentA = secx[n-1] + alignmentA
            alignmentB = '-' + alignmentB
            n -= 1
        elif(m > 0 and score[n,m] == score[n,m-1] + d):
            alignmentA = '-' + alignmentA
            alignmentB = secy[m-1] + alignmentB
            m -= 1

        

    return score, alignmentA, alignmentB
    
##########################
# Funcion Smith-Waterman #
##########################

def sw(secx, secy):
    n = len(secx)
    m = len(secy)
    d = -2
    x = 0
    y = 0
    mmax = 0
    
    score = np.zeros((n+1,m+1))

    for k in range(1,n+1):
        for l in range(1,m+1):
            
            match = score[k-1,l-1] + delta(secx[k-1],secy[l-1])
            delete = score[k-1,l] + d
            insert = score[k,l-1] + d
            score[k,l] = max(match,delete,insert,0)

            if score[k,l] >= mmax:
                mmax = score[k,l]
                x = k
                y = l

    alignmentA = ''
    alignmentB = ''

    while (score[x-1,y-1] != 0) or (score[x-1,y] != 0) or (score[x,y-1] != 0):
        if(x > 0 and y > 0 and score[x,y] == score[x-1,y-1] + delta(secx[x-1],secy[y-1])):
            alignmentA = secx[x-1] + alignmentA
            alignmentB = secy[y-1] + alignmentB
            x -= 1
            y -= 1
        elif(x > 0 and score[x,y] == score[x-1,y] + d):
            alignmentA = secx[x-1] + alignmentA
            alignmentB = '-' + alignmentB
            x -= 1
        elif(y > 0 and score[x,y] == score[x,y-1] + d):
            alignmentA = '-' + alignmentA
            alignmentB = secy[y-1] + alignmentB
            y -= 1


    return score, alignmentA, alignmentB


#################
# Funcion Delta #
#################

        
def delta(x,y,z = 1):
    if x == y:
        return z
    else:
        return -z

########
# Main #
########
    
if __name__ == '__main__':
    __main__()
