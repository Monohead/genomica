import matplotlib.pyplot as plt
from random import random, randint
from scipy import sqrt, zeros

def __main__():
    p = [(0.0,0.0),(0.5,sqrt(3)/2),(1.0,0.0)]
    N = 1000000
    x = zeros(N)
    y = zeros(N)

    x[0] = random()
    y[0] = random()

    print x[0],y[0]
    
    for i in range(1,N):
        k = randint(0,2)
        x[i], y[i] = puntoMedio(p[k],(x[i-1],y[i-1])) 

    plt.scatter(x,y,s=1)
    plt.show()

def puntoMedio(p,q):
    return (0.5*(p[0]+q[0]),0.5*(p[1]+q[1]))

if __name__ == '__main__':
    __main__()
