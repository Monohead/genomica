from scipy import zeros
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def __main__():

    f = open('genomas/titin.fasta')

    s = ''

    for line in f.readlines():
        s += line.strip()


    figura = plt.figure()
    ax = Axes3D(figura)
    
    x,y,z = genCoord(s)
    ax.scatter(x,y,z,s=1)
    plt.show()

def genCoord(gen):
    ls = agrupa_en_n(gen,100)
    x = zeros(len(ls))
    y = zeros(len(ls))
    z = zeros(len(ls))
    for i in range(0,len(ls)):
        x[i],y[i],z[i] = coord(ls[i])
    return x,y,z

def coord(sequence):
    sw = ws(sequence)
    yr = ry(sequence)
    mk = xor(sw,yr)

    sw = bin(int(sw, 2))
    yr = bin(int(yr, 2))
    mk = bin(int(mk, 2))

    xi = float(int(sw,2))
    yi = float(int(yr,2))
    zi = float(int(mk,2))
    
    return (xi,yi,zi)

def ws(sequence):
    sequence = sequence.replace('A','0')
    sequence = sequence.replace('T','0')
    sequence = sequence.replace('C','1')
    sequence = sequence.replace('G','1')

    return sequence

def ry(sequence):
    sequence = sequence.replace('A','0')
    sequence = sequence.replace('G','0')
    sequence = sequence.replace('C','1')
    sequence = sequence.replace('T','1')

    return sequence

def xor(firstSequence,secondSequence):
    res = ''
    for i in range(0,len(firstSequence)):
        if(firstSequence[i] == secondSequence[i]):
            res += '0'
        else:
            res += '1'
    return res

def agrupa_en_n(secuencia, n = 3):
    
    lista = [secuencia[i:i+n] for i in range(0, len(secuencia), n)]
    return lista
    
if __name__ == "__main__":
    __main__()
